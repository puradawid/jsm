/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pb.beans.api;

import java.util.List;
import javax.ejb.Local;
import pl.edu.pb.entities.UserStory;

/**
 *
 * @author Dawid
 */
@Local
public interface UserStoryManager {
    /**
     * Method adds userstory to bunch.
     * @param userStory Defined UserStory. Cannot be null.
     */
    public void addUserStory(UserStory userStory);
    
    /**
     * Method removes userstory from particular bunch.
     * @param userStory Story to remove.
     */
    public void removeUserStory(UserStory userStory);
    
    /**
     * Method replaces old story by new (modify or something).
     * @param replace Story which will be replaced.
     * @param replacedBy Story which will be new story iin bunch
     * 
     * FOR PROGRAMMER:
     * This method can be connected with removeUserStory and addUserStory. You
     * will need to check, if replace is really existing in bunch and replacedBy
     * is really new story. Thx.
     */
    public void replaceUserStory(UserStory replace, UserStory replacedBy);
    
    /**
     * Provides access to all userstories in bunch. 
     * REMEMBER, ROGRAMMER: that have to be new instance, not this same as
     * really instance of elements!
     * @return List of UserStory.
     */
    public List<UserStory> getAllUserStories();
    
}
