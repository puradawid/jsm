/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pb.beans.api.mocks;

import java.sql.Date;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import pl.edu.pb.beans.api.UserStoryManager;
import pl.edu.pb.entities.UserStory;

/**
 *
 * @author Dawid
 */
@Singleton
@LocalBean
public class UserStoryManagerMock implements UserStoryManager {

    List<UserStory> userStories = new ArrayList<UserStory>();

    public UserStoryManagerMock() {
        java.util.Calendar cal = java.util.Calendar.getInstance();
        java.util.Date utilDate = cal.getTime();
        java.sql.Date sqlDate = new Date(utilDate.getTime());
        UserStory nowy = new UserStory();
        nowy.setStory("Ale zajebista histora");
        nowy.setCategory("ale kategoriaaaa");
        nowy.setDatevalue(sqlDate);
        UserStory nowy2 = new UserStory();
        nowy2.setStory("Ale zajebista histora");
        nowy2.setCategory("ale kategoriaaaa");
        nowy2.setDatevalue(sqlDate);
        userStories = new ArrayList<UserStory>();
        userStories.add(nowy);
        userStories.add(nowy2);
        userStories.add(nowy2);

        userStories.add(nowy2);

        userStories.add(nowy2);

        userStories.add(nowy2);

        userStories.add(nowy2);

        userStories.add(nowy2);

        userStories.add(nowy2);

        userStories.add(nowy2);

        userStories.add(nowy2);

        userStories.add(nowy2);
        userStories.add(nowy2);

        userStories.add(nowy2);




    }

    @Override
    public void addUserStory(UserStory userStory) {
        if (userStory != null) {
            userStories.add(userStory);
        }
    }

    @Override
    public void removeUserStory(UserStory userStory) {
        userStories.remove(userStory);
    }

    @Override
    public void replaceUserStory(UserStory replace, UserStory replacedBy) {
        if (userStories.contains(replace) && !userStories.contains(replacedBy)) {
            userStories.remove(replace);
            userStories.add(replacedBy);
        }
    }

    @Override
    public List<UserStory> getAllUserStories() {
        return new LinkedList<UserStory>(userStories);
    }
}
