/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pb.beans;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import pl.edu.pb.beans.api.UserStoryManager;
import pl.edu.pb.entities.UserStory;
import pl.edu.pb.entities.beans.UserStoryFacade;

/**
 * Implementation of UserStoryManager interface.
 * It is really weak implementation assuming there is no problem with tasks.
 * @author Dawid
 */
@Stateless
public class UserStoryBean implements UserStoryManager{
    
    @EJB
    UserStoryFacade usf;
    
    @Override
    public void addUserStory(UserStory userStory) {
        usf.create(userStory);
    }

    @Override
    public void removeUserStory(UserStory userStory) {
        usf.remove(userStory);
    }

    @Override
    public void replaceUserStory(UserStory replace, UserStory replacedBy) {
        //checking of correctly instances?
        if(!usf.findAll().contains(replace)) 
            throw new RuntimeException("There is no story like this");
        usf.remove(replace);
        usf.create(replacedBy);
    }

    @Override
    public List<UserStory> getAllUserStories() {
        return usf.findAll();
    }
    
}
