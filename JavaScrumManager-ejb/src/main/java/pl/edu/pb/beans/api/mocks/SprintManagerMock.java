/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pb.beans.api.mocks;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import pl.edu.pb.beans.api.BucketManager;
import pl.edu.pb.beans.api.SprintManager;
import pl.edu.pb.beans.api.UserStoryManager;
import pl.edu.pb.beans.api.UserTasksManager;

/**
 *
 * @author Dawid
 */


@Singleton
@LocalBean
public class SprintManagerMock {
    @EJB
    private UserTasksManagerMock userTasksManagerMock;
    @EJB
    private BucketManagerMock bucketManagerMock;
    @EJB
    private UserStoryManagerMock userStoryManagerMock;

    /**
     * This enum really simple describes a state of sprint. In first action,
     * sprint is routed on take userStories from Product Owner. When PO will
     * end his job there will be a phase called "implementation of ideas". This
     * means ScrumMaster will rationalise all of stories and add to them tasks
     * for do. When he will done his job there will be a last phase o sprint -
     * getting things done.
     */
    private enum SprintStatus {
        userStory,
        generateTask,
        gettingJobDone
    }
    private SprintStatus status = SprintStatus.userStory;

    public void doneUserStories() {
        if(status == SprintStatus.userStory)
            status = SprintStatus.generateTask;
    }

    public void doneTasks() {
        if(status == SprintStatus.generateTask)
            status = SprintStatus.gettingJobDone;
    }

    public UserTasksManagerMock getUserTasksManagerMock() {
        return userTasksManagerMock;
    }

    public BucketManagerMock getBucketManagerMock() {
        return bucketManagerMock;
    }

    public UserStoryManagerMock getUserStoryManagerMock() {
        return userStoryManagerMock;
    }

    
}
