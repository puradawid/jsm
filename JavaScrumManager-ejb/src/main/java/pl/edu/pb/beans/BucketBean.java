/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pb.beans;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import pl.edu.pb.beans.api.BucketManager;
import pl.edu.pb.beans.api.SprintManager;
import pl.edu.pb.entities.Task;
import pl.edu.pb.entities.beans.AppUserFacade;
import pl.edu.pb.entities.beans.TaskFacade;
import pl.edu.pb.login.Credentials;

/**
 *
 * @author Mehi
 */
@Stateful
@LocalBean
public class BucketBean implements BucketManager {

    SprintManager sm;
    
    @PersistenceUnit(unitName = "pl.edu.pb_JavaScrumManager-ejb_ejb_1.0-SNAPSHOTPU")
    EntityManagerFactory emf;
    
    @EJB
    TaskFacade tf;
    
    @EJB
    LoggedUser user;
    
    protected SprintManager getSprintInstance()
    {
        InitialContext ic;
        try {
            ic = new InitialContext();
            sm = (SprintManager) ic.lookup(
                "java:global/JavaScrumManager-ear/JavaScrumManager-ejb-1.0-SNAPSHOT"
                + "/SprintBean!pl.edu.pb.beans.api.SprintManager");
        } catch (NamingException ex) {
            Logger.getLogger(BucketBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sm;
    }
    
    @Override
    public List<Task> getAll() {
        return tf.getUnassignedTasks();
    }

    @Override
    public void put(Task t) {
        t.setSprint(getSprintInstance().getCurrent());
        tf.create(t);
    }
    
    @Override
    public Task take(Task t) {
        t.setStartTime(new Date());
        return tf.takeTask(user.getUser(), t);
    }
}
