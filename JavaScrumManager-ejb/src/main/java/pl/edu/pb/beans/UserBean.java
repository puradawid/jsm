/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pb.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Schedule;
import javax.ejb.Startup;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import pl.edu.pb.entities.AppUser;
import pl.edu.pb.entities.UserContact;
import pl.edu.pb.entities.beans.AppUserFacade;
import pl.edu.pb.login.Credentials;
/**
 *
 * @author Dawid
 */

@SessionScoped
@Named
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class UserBean implements Serializable {
    
    private static final long serialVersionUID = 7965455427888195913L;
    
    @Resource
    UserTransaction utx;
    
    @PersistenceUnit(unitName = "pl.edu.pb_JavaScrumManager-ejb_ejb_1.0-SNAPSHOTPU")
    EntityManagerFactory emf;
    
    @EJB
    AppUserFacade auf;

    @Inject
    private Credentials credentials;

    private AppUser currentUser;

    public void login() throws Exception {

        AppUser user = auf.findUser(credentials.getUsername(), credentials.getPassword());

        if (user != null) {

            this.currentUser = user;

            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage("Welcome, " + currentUser.getContact().getFirstName()));

        }

    }

    public void logout() {

        FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage("Goodbye, " + currentUser.getContact().getFirstName()));

        currentUser = null;

    }

    public boolean isLoggedIn() {

        return currentUser != null;
    }

    @Produces
    public AppUser getCurrentUser() {

        return currentUser;

    }    
    
}
