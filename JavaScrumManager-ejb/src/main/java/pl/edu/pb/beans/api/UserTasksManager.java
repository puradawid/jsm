package pl.edu.pb.beans.api;

import java.util.List;
import javax.ejb.Local;
import pl.edu.pb.entities.Task;

/**
 * Interface represents methods provide user to manage own tasks. The instance is
 * different for all users.
 * @author Dawid
 */
@Local
public interface UserTasksManager {
    /**
     * Gets all user task in this moment.
     * @return List of avaible tasks.
     */
    public List<Task> getAll();
    /**
     * Ends pointed task in own collection.
     * @param t Ended task.
     */
    public void end(Task t);
    
    /**
     * Adds new task from bucket or self.
     * @param t Added task.
     */
    public void add(Task t);
}
