/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pb.beans.api.mocks;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import pl.edu.pb.beans.api.UserTasksManager;
import pl.edu.pb.entities.Sprint;
import pl.edu.pb.entities.Task;

/**
 *
 * @author Dawid
 */

@Singleton
@LocalBean
public class UserTasksManagerMock implements UserTasksManager{
    
    private List<Task> userTasks = new ArrayList<Task>();
    
    public UserTasksManagerMock(){
                java.util.Calendar cal = java.util.Calendar.getInstance();
        java.util.Date utilDate = cal.getTime();
        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
        Task t = new Task();
        t.setName("Misja Dawida123");
        t.setDescription("Informacje tajne");
        t.setComplete(false);
        t.setId((long) 1);
        userTasks.add(t);
        Task t2 = new Task();
        t2.setName("Misja Dawida321");
        t2.setDescription("Informacje tajne");
        t2.setComplete(false);
        t2.setId((long) 1);
        Sprint s = new Sprint();
        s.setStartTime(sqlDate);
        sqlDate.setYear(2014);
        s.setEndTime(sqlDate);
        s.setId((long) 1);
        t2.setSprint(s);
        userTasks.add(t2);
        userTasks.add(t2);
        userTasks.add(t2);
        userTasks.add(t2);
        userTasks.add(t2);
        userTasks.add(t2);
        userTasks.add(t2);
        userTasks.add(t2);
        userTasks.add(t2);
        userTasks.add(t2);
        userTasks.add(t2);
        userTasks.add(t2);
        userTasks.add(t2);
        userTasks.add(t2);
        userTasks.add(t2);
        
        
    }
    
    @Override
    public List<Task> getAll() {
        List<Task> taskList = new ArrayList<Task>();
        for(int i=0;i<userTasks.size();i++){
            if(userTasks.get(i).isComplete()==false){
                taskList.add(userTasks.get(i));
            }
        }
        return taskList;
    }
    public List<Task> getCompleted(){
        List<Task> taskList = new ArrayList<Task>();
        for(int i=0;i<userTasks.size();i++){
            if(userTasks.get(i).isComplete()==true){
                taskList.add(userTasks.get(i));
            }
        }
        return taskList;
    }
    @Override
    public void end(Task t) {
        if(!userTasks.contains(t)) 
            return;
         userTasks.remove(t);
         t.setEndTime(new Date());
         t.setComplete(true);
         userTasks.add(t);
    }

    @Override
    public void add(Task t) {
        if(!userTasks.contains(t))
            userTasks.add(t);
        throw new RuntimeException("There is this task once again!");
    }
    
}
