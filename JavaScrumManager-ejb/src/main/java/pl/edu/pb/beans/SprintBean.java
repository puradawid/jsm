/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pb.beans;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.ejb.LocalBean;
import javax.inject.Inject;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import pl.edu.pb.beans.api.BucketManager;
import pl.edu.pb.beans.api.SprintManager;
import pl.edu.pb.beans.api.UserStoryManager;
import pl.edu.pb.beans.api.UserTasksManager;
import pl.edu.pb.entities.Sprint;
import pl.edu.pb.entities.beans.SprintFacade;
import pl.edu.pb.entities.beans.TaskFacade;

/**
 *
 * @author Mehi
 */
@Stateful
public class SprintBean implements SprintManager {

    @Resource
    UserTransaction utx;
    
    @PersistenceUnit(unitName = "pl.edu.pb_JavaScrumManager-ejb_ejb_1.0-SNAPSHOTPU")
    EntityManagerFactory emf;
    
    @EJB
    SprintFacade sf;
    
    @EJB(beanName = "BucketBean")
    BucketManager bm;
    
    @EJB(beanName = "UserTasksBean")
    UserTasksManager utm;
    
    @EJB(beanName = "UserStoryBean")
    UserStoryManager usm;
    
    @Inject
    private UserBean user;
    
    
    //points on current, new sprint.
    Sprint current;
    
    @PostConstruct
    public void initialize()
    {
        current = sf.findCurrent();
    }
    
    @Override
    public void doneUserStories() {
        current.setUserStoriesDone(true);
    }

    @Override
    public void doneTasks() {
        current.setTasksDone(true);
    }

    @Override
    public BucketManager getBucket() {
        return bm;
    }

    @Override
    public UserTasksManager getUserTasksManager() {
        return utm;
    }

    @Override
    public UserStoryManager getUserStoryManager() {
        return usm;
    }

    @Override
    public Sprint getCurrent() {
        return current;
    }
}
