package pl.edu.pb.beans.api;

import java.util.List;
import javax.ejb.Local;
import pl.edu.pb.entities.Task;

/**
 * BucketManager interface allows client to manage tasks from bucket
 * It needs to be:
 * - self-independent from other instances
 * - be a singleton (not neccesarly) - constructor can call obvious one instance
 * - provide action for user (putting, showing and taking task) 
 * @author Dawid
 */
@Local
public interface BucketManager {
    
    /**
     * Provides for user full list of current tasks.
     * @return Full bunch of untaked tasks.
     */
    public List<Task> getAll();
    
    /**
     * Put new task to bucket from user. Constraints:
     * - user must be Product Owner
     * - there is no sprint
     * @param t Task to add.
     */
    public void put(Task t);
    
    /**
     * Taking existing task from bucket to user.
     * @param t Taked task.
     * @return This same task if exist, null if it is not.
     */
    public Task take(Task t);
    
}
