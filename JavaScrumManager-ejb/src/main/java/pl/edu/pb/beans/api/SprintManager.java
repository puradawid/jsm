/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pb.beans.api;

import java.util.List;
import pl.edu.pb.entities.Sprint;
import pl.edu.pb.entities.UserStory;

/**
 * Interface provides methods for sprint managing.
 * IMPORTANT: 
 * - this interface assume that sprint is only singleton, but you can to manage
 *   that by some SprintManagerFactory.
 * @author Dawid
 * @see pl.edu.pb.entities.Sprint
 */
public interface SprintManager {
    
    
    /**
     * Method provides end of creating userstories sended in sprint.
     * This is non-returning signal for element.
     */
    public void doneUserStories();
    
    /**
     * Sending a signal for elements to close adding and managing task for 
     * Scrum Owner.
     * CONSTRAINTS:
     * - only for Scrum Master.
     */
    public void doneTasks();
    
    /**
     * Returns bucket of tasks to current sprint.
     * @return 
     */
    public BucketManager getBucket();
    
    /**
     * This factory method returns user task manager. Contstraint only for users with ProductOwner
     * premission.
     * @return usertaskmanager
     */
    public UserTasksManager getUserTasksManager();
    
    /**
     * Factory method returns userstory manager if it is not ended. 
     * @return 
     */
    public UserStoryManager getUserStoryManager();
    
    /**
     * Returns current sprint for application (used for stamping elements).
     * @return current sprint
     */
    public Sprint getCurrent();
}

