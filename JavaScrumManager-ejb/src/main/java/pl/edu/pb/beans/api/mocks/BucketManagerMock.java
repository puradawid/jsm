/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pb.beans.api.mocks;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.inject.Inject;
import pl.edu.pb.beans.api.BucketManager;
import pl.edu.pb.entities.Task;

/**
 *
 * @author Dawid
 */
@Singleton
@LocalBean
public class BucketManagerMock implements BucketManager {

    List<Task> bucketTask;
    int count;
    @EJB
    UserTasksManagerMock mock;

    public BucketManagerMock() {
        count = 0;
        bucketTask = new ArrayList<Task>();
        Task t = new Task();
        t.setName("Misja Dawida");
        t.setDescription("Informacje tajne");
        t.setComplete(false);
        t.setId((long) count++);
        bucketTask.add(t);
        Task t2 = new Task();
        t2.setName("Misja Dawida2");
        t2.setDescription("Informacje tajne2");
        t2.setComplete(false);
        t2.setId((long) count++);
        bucketTask.add(t2);
        
        bucketTask.add(t2);
        bucketTask.add(t2);

        bucketTask.add(t2);

        bucketTask.add(t2);

        bucketTask.add(t2);

        bucketTask.add(t2);

        bucketTask.add(t2);

        bucketTask.add(t2);

        bucketTask.add(t2);

        bucketTask.add(t2);

        bucketTask.add(t2);

        bucketTask.add(t2);



    }

    @Override
    public List<Task> getAll() {
        return bucketTask;
    }

    @Override
    public void put(Task t) {
        bucketTask.add(t);
    }

    @Override
    public Task take(Task t) {
        if (bucketTask.contains(t)) {
            bucketTask.remove(t);
            for (int i = 0; i < bucketTask.size(); i++) {
                System.out.println(bucketTask.get(i).getName());
            }
            mock.add(t);
            return t;
        } else {
            return null;
        }
    }
}
