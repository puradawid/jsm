/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pb.beans;

import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.annotation.security.DenyAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import pl.edu.pb.entities.AppUser;
import pl.edu.pb.entities.beans.AppUserFacade;

/**
 *
 * @author Dawid
 */
@LocalBean
@Stateless
@DenyAll
@RolesAllowed({"Administrator", "Member", "ScrumMaster", "ProductOwner"})
public class LoggedUser {
    
    private AppUser user;
    private String name;
    private String status;
    
    @EJB
    AppUserFacade ub;
    
    @Resource
    SessionContext ctx;
    
//    @PostConstruct
//    public void initialize()
//    {
//        name = ctx.getCallerPrincipal().getName();
//        user = ub.findExistingUser(name);
//        Logger.getLogger(this.getClass().getName()).fine("Initialized as " + user);
//    }
    
    public AppUser getUser()
    {
//        if(user != null)
//            return user;
//        else
//        {
            user = ub.findExistingUser(ctx.getCallerPrincipal().getName());
            return user;
//        }
    }
}
