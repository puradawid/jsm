/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pb.beans;

import java.util.List;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.inject.Inject;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import pl.edu.pb.beans.api.UserTasksManager;
import pl.edu.pb.entities.Task;
import pl.edu.pb.entities.beans.TaskFacade;

/**
 *
 * @author Mehi
 */
@Stateful
@LocalBean
public class UserTasksBean implements UserTasksManager {

    @Resource
    UserTransaction utx;
    @PersistenceUnit(unitName = "pl.edu.pb_JavaScrumManager-ejb_ejb_1.0-SNAPSHOTPU")
    EntityManagerFactory emf;
    @EJB
    TaskFacade tf;

    @EJB
    LoggedUser user;
    
    @Override
    public List<Task> getAll() {
        return tf.getUncompletedUserTasks(user.getUser());
    }
    
    public List<Task> getCompleted(){
        return tf.getCompletedUserTasks(user.getUser());
    }

    @Override
    public void end(Task t) {
        tf.endTask(t);
    }

    @Override
    public void add(Task t) {
        t.setWorker(user.getUser());
        tf.create(t);
    }
}
