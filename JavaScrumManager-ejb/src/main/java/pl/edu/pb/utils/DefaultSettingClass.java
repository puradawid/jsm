/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pb.utils;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.transaction.UserTransaction;
import pl.edu.pb.entities.AppUser;
import pl.edu.pb.entities.Sprint;
import pl.edu.pb.entities.UserContact;
import pl.edu.pb.entities.beans.AppUserFacade;
import pl.edu.pb.entities.beans.SprintFacade;

/**
 * This bean used to initialize data for example database of tasks. In general
 * purpose it is used to initiaize admin account and managing users and members.
 * Thank you for your cooperation.
 * @author Dawid
 */
@Singleton
@Startup
public class DefaultSettingClass {
    
    @EJB
    AppUserFacade uf;
    
    @EJB
    SprintFacade sf;
    
    @Resource
    UserTransaction utx;
    
    protected Sprint createFirstSprint()
    {
        Sprint s = new Sprint();
        s.setStartTime(new Date());
        return s;
    }
    
    protected AppUser createUser(String login, String firstname, String secondname,
            String passwrod, String mail, String telephone, String status)
    {
        AppUser result = new AppUser();
        result.setContact(new UserContact());
        result.setLogin(login);
        result.setPassword(passwrod);
        result.setStatus(status);
        result.getContact().setEmail(mail);
        result.getContact().setFirstName(firstname);
        result.getContact().setSecondName(secondname);
        return result;
    }
    
    protected List<AppUser> createStandardBunch()
    {
        List<AppUser> res = new LinkedList<AppUser>();
        
        res.add(createUser(
                "admin", "Administrator", "Default", "admin", "adminmail@mail.com", "666 666 666", "Administrator"));
        res.add(createUser(
                "po", "ProductOwner", "Kovalski", "po", "po@mycompany.com", "777 777 777", "ProductOwner"));
        res.add(createUser(
                "sm", "ScrumMaster", "Pietrzyk", "sm", "sm@mycompany.com", "888 888 888", "ScrumMaster"));
        res.add(createUser(
                "member", "Programmer", "Noname", "member", "member@mycompany.com", "999 999 999", "Member"));
        return res;
    }
    
    
    @PostConstruct
    protected void initialize()
    {
        try {
        //utx.begin();
        for(AppUser ap : createStandardBunch())
            uf.create(ap);
        sf.create(createFirstSprint());
        //utx.commit();
        } catch (Exception e)
        {
            Logger.getLogger(this.getClass().getName())
                    .severe("Transaction problem occured: " + e.getMessage());
        }
    }
    
}
