/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pb.login;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author Mehi
 */
@RequestScoped

@Named

public class Credentials {


   private String username;
   private String password;


   public String getUsername() {

      return username;

   }



   public void setUsername(String username) {

      this.username = username;

   }



   public String getPassword() {

      return password;

   }



   public void setPassword(String password) {

      this.password = password;

   }



}

