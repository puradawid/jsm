/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pb.entities.beans;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import pl.edu.pb.entities.AppUser;

/**
 *
 * @author Dawid
 */
@Stateless
public class AppUserFacade extends AbstractFacade<AppUser> {
    @PersistenceContext(unitName = "pl.edu.pb_JavaScrumManager-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AppUserFacade() {
        super(AppUser.class);
    }
    
    public AppUser findUser(String username, String password){
        AppUser result = (AppUser) em

                  .createQuery(
                        "select u from AppUser u where u.contact.firstName=:username and u.contact.password=:password")
                  .setParameter("username", username)
                  .setParameter("password", password).getSingleResult();
        return result;
    }
    
    public AppUser findExistingUser(String username){
        AppUser result = null;
        try {
        result = (AppUser) em
                  .createQuery(
                        "select u from AppUser u where u.login=:username")
                  .setParameter("username", username).getSingleResult();
        } catch (NoResultException e)
        {
            result = null; //for some strange reason i put nulling again (why?)
            //add Logger Comment here?
        }
        return result;
    }
    
}
