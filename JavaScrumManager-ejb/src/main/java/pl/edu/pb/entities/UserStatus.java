/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pb.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author Dawid
 */
@IdClass(UserStatusId.class)
@Entity
public class UserStatus implements Serializable
{
    @Id
    private String status;
    
    @Id
    @ManyToOne
    @JoinColumn(name = "login")
    private AppUser appUser;
    
//    public void set(String status, List<AppUser> users)
//    {
//        this.status = status;
//        this.appUsers = users;
//    }
//    
//    public List<AppUser> getAppUsers()
//    {
//        return appUsers;
//    }
}
