/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pb.entities.beans;

import java.sql.Date;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import pl.edu.pb.entities.AppUser;
import pl.edu.pb.entities.Task;

/**
 *
 * @author Dawid
 */
@Stateless
public class TaskFacade extends AbstractFacade<Task> {

    @PersistenceContext(unitName = "pl.edu.pb_JavaScrumManager-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TaskFacade() {
        super(Task.class);
    }

    public void endTask(Task t) {
        java.util.Calendar cal = java.util.Calendar.getInstance();
        java.util.Date utilDate = cal.getTime();
        java.sql.Date sqlDate = new Date(utilDate.getTime());
        em.createQuery("update Task t set t.endTime=:date, t.complete=true where t=:task")
                .setParameter("date", sqlDate)
                .setParameter("task", t)
                .executeUpdate();
    }

    public List<Task> getCompletedUserTasks(AppUser user) {
        List<Task> result = em
                .createQuery(
                "select t from Task t where t.worker=:user and t.complete = true")
                .setParameter("user", user).getResultList();
        return result;
    }

    public List<Task> getUncompletedUserTasks(AppUser user) {
        List<Task> result = em
                .createQuery(
                "select t from Task t where t.worker=:user and t.complete = false")
                .setParameter("user", user).getResultList();
        return result;
    }

    public List<Task> getUnassignedTasks() {
        List<Task> result = em
                .createQuery(
                "select t from Task t where t.worker is NULL").getResultList();
        return result;
    }

    public Task takeTask(AppUser user, Task t) {
        em.createQuery("update Task t set t.worker=:user where t=:task")
                .setParameter("user", user)
                .setParameter("task", t)
                .executeUpdate();
        Logger.getLogger(this.getClass().getName())
                .fine("User " + user.getLogin() + "takes task " + t.getName());
        return t;
    }
}
