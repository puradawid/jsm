/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pb.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Dawid
 */
@Entity
public class Task implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String description;
    @ManyToOne
    private Sprint sprint;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date startTime;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date endTime;
    @ManyToOne
    private TaskLog log;
    @OneToOne
    private UserStory origin;
    @ManyToOne
    private Task inheritance;
    @OneToMany
    private List<Task> parenthis;
    @ManyToOne
    private AppUser worker;
    boolean complete;
    
    public void setName(String name)
    {
        this.name = name;
    }        
    
    public String getName()
    {
        return name;
    }
    
    public String getDescription()
    {
        return description;
    }
    
    public void setDescription(String description)
    {
        this.description = description;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Sprint getSprint() {
        return sprint;
    }

    public void setSprint(Sprint sprint) {
        this.sprint = sprint;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public TaskLog getLog() {
        return log;
    }

    public void setLog(TaskLog log) {
        this.log = log;
    }

    public UserStory getOrigin() {
        return origin;
    }

    public void setOrigin(UserStory origin) {
        this.origin = origin;
    }

    public Task getInheritance() {
        return inheritance;
    }

    public void setInheritance(Task inheritance) {
        this.inheritance = inheritance;
    }

    public List<Task> getParenthis() {
        return parenthis;
    }

    public void setParenthis(List<Task> parenthis) {
        this.parenthis = parenthis;
    }

    public AppUser getWorker() {
        return worker;
    }

    public void setWorker(AppUser worker) {
        this.worker = worker;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public List<Task> splitTo(List<Task> newTasks) {
        for (Task t : newTasks) {
            t.setInheritance(this);
            getParenthis().add(t);
        }
        return newTasks;
    }

}
