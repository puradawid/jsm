/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pb.entities.beans;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import pl.edu.pb.entities.UserContact;

/**
 *
 * @author Dawid
 */
@Stateless
public class UserContactFacade extends AbstractFacade<UserContact> {
    @PersistenceContext(unitName = "pl.edu.pb_JavaScrumManager-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserContactFacade() {
        super(UserContact.class);
    }
    
}
