/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pb.entities.beans;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NamedQuery;
import javax.persistence.PersistenceContext;
import pl.edu.pb.entities.Sprint;

/**
 *
 * @author Dawid
 */
@Stateless
public class SprintFacade extends AbstractFacade<Sprint> {
    @PersistenceContext(unitName = "pl.edu.pb_JavaScrumManager-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SprintFacade() {
        super(Sprint.class);
    }
    
    //ADDED FIND CURRENT
   /**
    * Returns max valued sprint (correctly to current (assumption)).
    * @return Current Sprint instance.
    */
    public Sprint findCurrent()
    {
        return (Sprint)(getEntityManager()
                .createNamedQuery("Sprint.findCurrent").getSingleResult());
    }
}
