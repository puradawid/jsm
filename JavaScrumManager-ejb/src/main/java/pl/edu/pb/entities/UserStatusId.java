/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pb.entities;

import java.io.Serializable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author Dawid
 */
public class UserStatusId implements Serializable {
//   @ManyToOne
//   @JoinColumn(name = "login")
   private String appUser;
   
   private String status;
   
   @Override
   public boolean equals(Object o)
   {
       if(o.getClass() != UserStatusId.class) return false;
       UserStatusId u = (UserStatusId)o;
       return u.appUser.equals(this.appUser) && u.status.equals(this.status);
   }
   
   @Override
   public int hashCode()
   {
       return super.hashCode();
   }
}
