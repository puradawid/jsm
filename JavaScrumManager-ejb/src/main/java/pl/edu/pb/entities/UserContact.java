/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pb.entities;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 *
 * @author Dawid
 */
@Entity
public class UserContact implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
            
    String firstName;
    
    String secondName;
    
    String phoneNumber;
    
    String email;
    
    String password;
    
    public UserContact(String firstName, String secondName, String email,
            String password, String phoneNumber)
    {
        this.firstName = firstName;
        this.email = email;
        this.secondName = secondName;
        this.password = password;
        this.phoneNumber = phoneNumber;
    }
    
    public UserContact(){}
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }

    public String getPassword() {
        if(password == null) return "";
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    
    public String getFirstName() {
        if(firstName == null) return "";
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        if(secondName == null) return "";
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getPhoneNumber() {
        if(phoneNumber == null) return "";
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        if(email == null) return "";
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
