package pl.edu.pb.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

/**
 * Sprint class represents scrum entity
 * @author Dawid
 */

@Entity
@NamedQuery(name = "Sprint.findCurrent", query = "SELECT s FROM Sprint s WHERE s.id = (SELECT MAX(sp.id) FROM Sprint sp)")
public class Sprint implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    
    String review;
    @Temporal(javax.persistence.TemporalType.DATE)
    Date startTime;

    @Temporal(javax.persistence.TemporalType.DATE)
    Date endTime;
    
    @OneToMany
    List<UserStory> userStories;
    
    boolean userStoriesDone;
    
    boolean tasksDone;
    
    @OneToMany
    List<Task> tasks;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public List<UserStory> getUserStories() {
        return userStories;
    }

    public void setUserStories(List<UserStory> userStories) {
        this.userStories = userStories;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }
    
    
    public int calculateLeftTime()
    {
        return (int)(((new Date()).getTime() - getEndTime().getTime())/60000); 
    }
    
    public List<Task> getCompleteTasks()
    {
        List<Task> complete = new LinkedList<Task>();
        for(Task t : getTasks())
            if(t.isComplete())
                complete.add(t);
        return complete;
    }
    
    public List<Task> getUnCompleteTasks()
    {
        List<Task> uncomplete = new LinkedList<Task>();
        for(Task t : getTasks())
            if(t.isComplete() == false)
                uncomplete.add(t);
        return uncomplete;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public boolean isUserStoriesDone() {
        return userStoriesDone;
    }

    public void setUserStoriesDone(boolean userStoriesDone) {
        this.userStoriesDone = userStoriesDone;
    }

    public boolean isTasksDone() {
        return tasksDone;
    }

    public void setTasksDone(boolean tasksDone) {
        this.tasksDone = tasksDone;
    }
    
    
    
}
