/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pb.entities.beans;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import pl.edu.pb.entities.UserStory;

/**
 *
 * @author Dawid
 */
@Stateless
public class UserStoryFacade extends AbstractFacade<UserStory> {
    @PersistenceContext(unitName = "pl.edu.pb_JavaScrumManager-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserStoryFacade() {
        super(UserStory.class);
    }
    
}
