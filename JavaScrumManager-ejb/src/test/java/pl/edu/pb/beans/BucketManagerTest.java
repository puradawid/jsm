package pl.edu.pb.beans;

import javax.ejb.EJB;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;
import javax.naming.NamingException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import pl.edu.pb.beans.api.BucketManager;

/**
 *
 * @author Dawid
 */
public class BucketManagerTest {
    
    private static EJBContainer container;
    
    private static Context context;
    
    BucketManager bucketManager;
    
    public BucketManagerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        container = EJBContainer.createEJBContainer();
        context = container.getContext();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    public void constructTest(){
        try 
        {
        context.lookup("java:global/classes/BucketManagerMock");
        } catch (NamingException e)
        {
            System.out.println(e.getMessage());
        }
    }
    
    
    @After
    public void tearDown() {
    }
    
}