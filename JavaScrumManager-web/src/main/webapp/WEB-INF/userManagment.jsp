<%-- 
    Document   : userManagment
    Created on : 2013-06-10, 23:05:16
    Author     : Dawid
--%>
<%@page import="pl.edu.pb.entities.AppUser"%>
<%@page import="java.util.List"%>
<%List<AppUser> users = (List<AppUser>)request.getAttribute("users");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>User Managment Page</title>
    </head>
    <body>
        <%if(request.getAttribute("message") != null) {%>
            <p><%=request.getAttribute("message")%></p>
        <% } %>
        <h1>User Managment</h1>
        
        <h2>Users</h2> 
        <table border="1">
            <tr>
                <td>First name</td>
                <td>Second name</td>
                <td>Username</td>
                <td>Password</td>
                <td>Phone number</td>
                <td>Email address</td>
                <td>Status</td>
                <td>Functionallity</td>
            </tr>
            
            <% if(users != null) {%>
            <% for(AppUser user : users) { %>
            <tr>
                <% if(user.getContact() != null) { %>
                <td><%=user.getContact().getFirstName()%></td>
                <td><%=user.getContact().getSecondName()%></td>
                <td><%=user.getLogin()%></td>
                <td><%=user.getPassword()%></td>
                <td><%=user.getContact().getPhoneNumber()%></td>
                <td><%=user.getContact().getEmail()%></td>
                <td><%=user.getStatus()%></td>
                <td>
                    <form method="post">
                        <input type="hidden" name="username" value="<%=user.getLogin()%>" />
                        <input type="hidden" name="rebindhtmlmethod" value="delete" />
                        <input type="submit" value="Delete him/her!" />
                    </form>
                </td>
                <% } %>
            </tr>
            <% }} %>
        </table>
        
        <!-- Form to add some users -->
        <h2>Add user</h2> 
        <form action="users" method="POST">
            <table border="1">
                <tr>
                    <td>First name</td>
                    <td>Second name</td>
                    <td>Username</td>
                    <td>Password</td>
                    <td>Phone number</td>
                    <td>Email address</td>
                    <td>Status</td>
                </tr>
                <tr>
                    <td><input type="text" name="firstname" /></td>
                    <td><input type="text" name="secondname" /></td>
                    <td><input type="text" name="username" /></td>
                    <td><input type="text" name="password" /></td>
                    <td><input type="text" name="phonenumber" /></td>
                    <td><input type="text" name="email" /></td>
                    <td>
                        <select name="status">
                            <option value="Member">Member</option>
                            <option value="ProductOwner">ProductOwner</option>
                            <option value="ScrumMaster">ScrumMaster</option>
                            <option value="Administrator">Administrator</option>
                        </select>
                    </td>
                    <td><input type="submit" value="Create" /></td>
                </tr>
            </table>
            
        </form>
    </body>
</html>
