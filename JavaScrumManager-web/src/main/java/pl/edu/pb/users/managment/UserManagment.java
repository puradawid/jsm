/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pb.users.managment;

import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;
import pl.edu.pb.entities.AppUser;
import pl.edu.pb.entities.beans.AppUserFacade;
import pl.edu.pb.entities.beans.UserContactFacade;

/**
 *
 * @author Dawid
 */
@Stateless
@RolesAllowed("Administrator")
public class UserManagment {
   
      @EJB(beanName = "AppUserFacade")
      AppUserFacade auf;
      @EJB(beanName = "UserContactFacade")
      UserContactFacade ucf;
    
    
    
    public void addUser(AppUser user)
    {
        //oh i think it is really crap
        //ucf.create(user.getContact());
        auf.create(user);
    }
    
    public void deleteUser(String name)
    {
        List<AppUser> users = auf.findAll();
        for(AppUser au : users)
            if(au.getLogin().equals(name))
                auf.remove(au);
    }
    
    public boolean isUserExist(String username)
    {
        return auf.findExistingUser(username) != null;
    }
    public List<AppUser> getAll()
    {
        return auf.findAll();
    }
}
