package pl.edu.pb.gui.servlets;

import java.io.IOException;
import javax.annotation.security.DenyAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pl.edu.pb.users.managment.UserManagment;
import pl.edu.pb.entities.AppUser;
import pl.edu.pb.entities.UserContact;

/**
 * UserManagerServlet class provides web-controlled user managment.
 * Requested credentials: Administrator
 * Web-generated: JSP
 * @author Dawid
 */
@DenyAll
@RolesAllowed("Administrator")
public class UserManagerServlet extends HttpServlet {
    @EJB
    private UserManagment userManagment;
    
    @Override
    @RolesAllowed("Administrator")
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doget");
        req.setAttribute("users", userManagment.getAll());
        System.out.println(userManagment.getAll());
        req.getRequestDispatcher("/WEB-INF/userManagment.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
            throws ServletException, IOException {
        String rebind = req.getParameter("rebindhtmlmethod");
        //Ok it is a tricky part. In fact browsers cant use DELETE, so i have to
        //rebind this through POST part. This is not really good, but what i should do?
        if(rebind != null && rebind.equals("delete"))
        {
            doDelete(req,resp);
            return;
        }
        
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        //logger
        System.out.println("adding " + username + ":" + password);
        
        AppUser newUser = new AppUser();
        UserContact userContact = new UserContact();
        if(username != null && password != null && !username.equals("") &&
                !password.equals(""))
        {
            if(userManagment.isUserExist(username))
            {
                req.setAttribute("message", "That user exist already in out db");
            } else
            {
                userContact.setFirstName(req.getParameter("firstname"));
                userContact.setEmail(req.getParameter("email"));
                userContact.setPhoneNumber(req.getParameter("phonenumber"));
                userContact.setSecondName(req.getParameter("secondname"));
                newUser.setLogin(username);
                newUser.setPassword(password);
                newUser.setStatus(req.getParameter("status"));
                newUser.setContact(userContact);
                userManagment.addUser(newUser);
                req.setAttribute("message", "User was added, thank you for cooperation!");
            }
        }
        else
            req.setAttribute("message", "Have you forgot about username and passwrod?!"
                    + "Bad, bad admin!");
        doGet(req, resp);
    }
    
    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = (String)req.getParameter("username");
        
        if(username != null)
        {
            if(userManagment.isUserExist(username))
            {
                userManagment.deleteUser(username);
                req.setAttribute("message", "Ok, user " + username + " have been deleted. Are you happy?");
            } else 
            req.setAttribute("message", "That user is not exist in out db! Come on man, what did you do?!");
        }
        else
        {
            req.setAttribute("message", "Request have no username. Reported and logged, you hacker.");
            //report and logger indeed @LoggerInside!
        }
        doGet(req, resp);
    }
    
    
    
}
