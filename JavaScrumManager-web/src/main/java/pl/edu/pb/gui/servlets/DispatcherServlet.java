/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pb.gui.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pl.edu.pb.beans.api.BucketManager;
import pl.edu.pb.entities.beans.SprintFacade;

/**
 *
 * @author Dawid
 */
public class DispatcherServlet extends HttpServlet {
    @EJB
    private SprintFacade sprintFacade;
    
    //BucketManager bucketManagerMock = lookupBucketManagerMockLocal();
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter pw = resp.getWriter();
        System.out.println(sprintFacade);
        System.out.println(req.getRequestURI());
        String[] request  = processURI(req.getRequestURI());
        if(request.length <= 1)
        {
            if(req.getUserPrincipal() == null)
                req.getRequestDispatcher("/login").forward(req, resp);
            req.getRequestDispatcher("/faces/Dashboard.xhtml").forward(req, resp);
            return;
        }
        if(request[1].equals("dashboard"))
            req.getRequestDispatcher("/faces/Dashboard.xhtml").forward(req, resp);
        if(request[1].equals("login"))
            req.getRequestDispatcher("/faces/LoginScreen.xhtml").forward(req, resp);
        if(request[1].equals("users"))
            req.getRequestDispatcher("/users").forward(req, resp);
        if(request[1].equals("logout"))
        {
            req.logout();
            req.getRequestDispatcher("/login").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
    
    
    
    protected String[] processURI(String uri)
    {
        int strings = 0;
        if(uri.length() <= 1)
            return new String[0];
        for(int i = 0; i < uri.length(); i++)
        {
            if(uri.charAt(i) == '/' && i != uri.length() - 1)
                strings++;
        }
        String[] result = new String[strings];
        int j = 0;
        for(int i = 0; i < uri.length(); i++)
        {
            if(uri.charAt(i) == '/' ){
                if(i == uri.length() - 1) continue;
                result[j] = new String();
                System.out.println(j);
                j++;
            }
            else
                result[j - 1] = result[j - 1].concat(new String(uri.substring(i, i+1)));
        }
        return result;
        
    }

    private BucketManager lookupBucketManagerMockLocal() {
        try {
            Context c = new InitialContext();
            return (BucketManager) c.lookup("java:global/JavaScrumManager-ear/JavaScrumManager-ejb-1.0-SNAPSHOT/BucketManagerMock!pl.edu.pb.beans.api.BucketManager");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
}
