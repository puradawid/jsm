package pl.pb.edu.gui.beans;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.Serializable;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.DashboardReorderEvent;
import org.primefaces.model.DashboardColumn;
import org.primefaces.model.DashboardModel;
import org.primefaces.model.DefaultDashboardColumn;
import org.primefaces.model.DefaultDashboardModel;

/**
 *
 * @author Mehi
 */
@Named(value = "dashboardBean")
@RolesAllowed({"Administrator", "Member", "ProductOwner", "ScrumMaster"})
@ApplicationScoped
public class DashboardBean implements Serializable {

	private DashboardModel model;
	private int columnCount;
        private DashboardColumn[] columns;
        public DashboardBean() {
                columns = new DashboardColumn[4];
		model = new DefaultDashboardModel();
		for(int i=0;i<4;i++){
                    columns[i] = new DefaultDashboardColumn();
                }

                columns[0].addWidget("DashboardControl");
                        
                for(int i=0;i<4;i++){
                    model.addColumn(columns[i]);
                }
                columnCount = 4;
	}
	@RolesAllowed({"Administrator", "Member", "ProductOwner", "ScrumMaster"})
	public void handleReorder(DashboardReorderEvent event) {
		FacesMessage message = new FacesMessage();
		message.setSeverity(FacesMessage.SEVERITY_INFO);
		message.setSummary("Reordered: " + event.getWidgetId());
		message.setDetail("Item index: " + event.getItemIndex() + ", Column index: " + event.getColumnIndex() + ", Sender index: " + event.getSenderColumnIndex());
		addMessage(message);
	}
	
	@RolesAllowed({"Administrator", "Member", "ProductOwner", "ScrumMaster"})
	private void addMessage(FacesMessage message) {
		FacesContext.getCurrentInstance().addMessage(null, message);
	}
	@RolesAllowed({"Administrator", "Member", "ProductOwner", "ScrumMaster"})
	public DashboardModel getModel() {
		return model;
	}
    @RolesAllowed({"Administrator", "Member", "ProductOwner", "ScrumMaster"})
    public int getColumnCount() {
        return columnCount;
    }

    public void setColumnCount(int columntCount) {
        this.columnCount = columntCount;
    }
    
    public void addBucket(ActionEvent ae){
        addWidget("Bucket");
    }
    public void addUserTasks(ActionEvent ae){
        addWidget("UserTasks");
    }
    public void addCompletedTasks(ActionEvent ae){
        addWidget("CompletedTasks");
    }
    public void addTime(ActionEvent ae){
        addWidget("Time");
    }
    public void addScrumReview(ActionEvent ae){
        addWidget("ScrumReview");
    }
    public void addUserStory(ActionEvent ae){
        addWidget("UserStory");
    }
    public void closeBucket(CloseEvent ce){
        addWidget("Bucket");
    }
    private void addWidget(String name){
        boolean found = false;
        int x = 0;
        boolean slotFound = false;
        for(int i=0;i<4;i++){
            List<String> widgetList = columns[i].getWidgets();
            if(widgetList.size()<3 && slotFound == false){
                slotFound = true;
                x = i;
            }
            for(int j=0;j<widgetList.size();j++){
                if(widgetList.get(j).equals(name)){
                    columns[i].removeWidget(name);
                    found = true;
                }
            }
        }
        if(found == false && slotFound == true){
            columns[x].addWidget(name);
        }
    }
        
}
			
