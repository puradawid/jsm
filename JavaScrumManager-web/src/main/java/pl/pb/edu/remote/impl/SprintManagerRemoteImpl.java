package pl.pb.edu.remote.impl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.annotation.security.DenyAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Startup;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import pl.edu.pb.beans.LoggedUser;
import pl.edu.pb.beans.api.BucketManager;
import pl.edu.pb.beans.api.SprintManager;
import pl.edu.pb.beans.api.UserTasksManager;
import pl.edu.pb.beans.api.mocks.BucketManagerMock;
import pl.edu.pb.beans.api.mocks.SprintManagerMock;
import pl.edu.pb.entities.Task;
import pl.edu.pb.entities.UserStory;

/**
 *
 * @author Mehi
 */
@ManagedBean
@SessionScoped
@DenyAll
@RolesAllowed({"Administrator", "ProductOwner", "Member"})
public class SprintManagerRemoteImpl implements Serializable {

    /**
     * Creates a new instance of SprintManager
     */
    @EJB(beanName = "SprintBean")
    private SprintManager sprintManagerMock;
    
    private List<Task> bucketList;
    private Task selectedTask;
    private String name, description;
    private Task userSelectedTask;
    private List<UserStory> userStoryList;
    private List<Task> taskList;
    private List<Task> completedTaskList;
    private String story, category;
    private UserStory userStorySelected;
    
    @EJB(lookup = "java:global/JavaScrumManager-ear/JavaScrumManager-ejb-1.0-SNAPSHOT/LoggedUser")
    private LoggedUser lu;
    
    public SprintManagerRemoteImpl() throws NamingException {
//        if (sprintManagerMock == null) {
//            InitialContext ic = new InitialContext();
//        }
        bucketList = new ArrayList<Task>();
        taskList = new ArrayList<Task>();
        completedTaskList = new ArrayList<Task>();
        userStoryList = new ArrayList<UserStory>();
    }
    // Bucket

    public List<Task> getAllBucket() {
        bucketList = sprintManagerMock.getBucket().getAll();
        return bucketList;
    }

    public void putBucket(ActionEvent ae) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (!name.equals("") && !description.equals("")) {
            Task t = new Task();
            t.setDescription(description);
            t.setName(name);
            sprintManagerMock.getBucket().put(t);
            setName("");
            setDescription("");
            context.addMessage(null, new FacesMessage("Success", "The task has been added to bucket."));
        } else {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning", "Fill required fields!"));
        }
    }

    public void takeBucket(ActionEvent action) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (selectedTask == null) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning", "No selection!"));
        } else {
            sprintManagerMock.getBucket().take(selectedTask);
            bucketList.remove(selectedTask);
            selectedTask = null;
            System.out.println("asd");
            context.addMessage(null, new FacesMessage("Success", "The task is now yours"));
        }
    }

    //User Tasks
    public List<Task> getAllUserTask() {
        taskList = sprintManagerMock.getUserTasksManager().getAll();
        return taskList;
    }

    public void endUserTask(ActionEvent actionEvent) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (userSelectedTask == null) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning", "No selection!"));
        } else {
            taskList.remove(userSelectedTask);
            completedTaskList.add(userSelectedTask);
            sprintManagerMock.getUserTasksManager().end(userSelectedTask);
            userSelectedTask = null;
            context.addMessage(null, new FacesMessage("Success", "The task is now completed."));
        }

    }

    public void addUserTask(Task t) {
        sprintManagerMock.getUserTasksManager().add(t);
    }
    // User story

    public void addUserStory(ActionEvent actionEvent) {
        FacesContext context = FacesContext.getCurrentInstance();

        if (story.equals("") || category.equals("")) {
        } else {
            java.util.Calendar cal = java.util.Calendar.getInstance();
            java.util.Date utilDate = cal.getTime();
            java.sql.Date sqlDate = new Date(utilDate.getTime());
            UserStory userStory = new UserStory();
            userStory.setCategory(category);
            userStory.setStory(story);
            userStory.setDatevalue(sqlDate);
            sprintManagerMock.getUserStoryManager().addUserStory(userStory);
            setCategory("");
            setStory("");
            context.addMessage(null, new FacesMessage("Success", "User story has been added."));
        }
    }

    public void removeUserStory(ActionEvent actionEvent) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (userStorySelected == null) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning", "No selection!"));
        } else {
            userStoryList.remove(userStorySelected);
            sprintManagerMock.getUserStoryManager().removeUserStory(userStorySelected);
            userStorySelected = null;
            context.addMessage(null, new FacesMessage("Success", "User story has been removed."));
        }
    }

    public void replaceUserStory(UserStory replace, UserStory replacedBy) {
        sprintManagerMock.getUserStoryManager().replaceUserStory(replace, replacedBy);
    }

    public List<UserStory> getAllUserStories() {
        userStoryList = sprintManagerMock.getUserStoryManager().getAllUserStories();
        return userStoryList;
    }

    public List<Task> getBucketList() {
        bucketList = sprintManagerMock.getBucket().getAll();
        return bucketList;
    }

    public void setBucketList(List<Task> bucketList) {
        this.bucketList = bucketList;
    }

    public Task getSelectedTask() {
        return selectedTask;
    }

    public void setSelectedTask(Task selectedTask) {
        this.selectedTask = selectedTask;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Task getUserSelectedTask() {
        return userSelectedTask;
    }

    public void setUserSelectedTask(Task userSelectedTask) {
        this.userSelectedTask = userSelectedTask;
    }

    public List<UserStory> getUserStoryList() {
        userStoryList = sprintManagerMock.getUserStoryManager().getAllUserStories();
        return userStoryList;
    }

    public void setUserStoryList(List<UserStory> userStoryList) {
        this.userStoryList = userStoryList;
    }

    public List<Task> getTaskList() {
        taskList = sprintManagerMock.getUserTasksManager().getAll();
        return taskList;
    }

    public void setTaskList(List<Task> taskList) {
        this.taskList = taskList;
    }

    public List<Task> getCompletedTaskList() {
        completedTaskList = sprintManagerMock.getUserTasksManager().getAll(); //completed?
        return completedTaskList;
    }

    public void setCompletedTaskList(List<Task> completedTaskList) {
        this.completedTaskList = completedTaskList;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public UserStory getUserStorySelected() {
        return userStorySelected;
    }

    public void setUserStorySelected(UserStory userStorySelected) {
        this.userStorySelected = userStorySelected;
    }
}
