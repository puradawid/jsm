/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pb.edu.remote.impl;

import javax.ejb.EJB;
import javax.inject.Named;
import pl.edu.pb.beans.LoggedUser;

/**
 *
 * @author Dawid
 */
@Named(value = "userSession")
public class UserSession {
    
    @EJB
    LoggedUser lu;
    
    public boolean getIsNotAllowedToUserStories()
    {
        String status = lu.getUser().getStatus();
        return !(status.equals("ProductOwner") || status.equals("ScrumMaster"));
    }
    
    public void setIsNotAllowedToUserStories(boolean value)
    {
        //nothing to do here? :-(
    }
    
    public boolean getIsNotAllowedToBucket()
    {
        String status = lu.getUser().getStatus();
        return status.equals("ProductOwner");
    }
    
    public void setIsAllowedToBucket(boolean value)
    {
        //nothing to do here? :-(
    }
    
    public boolean getIsNotAllowedToUserTasks()
    {
        String status = lu.getUser().getStatus();
        return status.equals("ProductOwner");
    }
    
    public void setIsAllowedToUserTasks(boolean value)
    {
        //nothing to do here? :-(
    }
    public boolean getIsNotAllowedToCompletedTasks()
    {
        String status = lu.getUser().getStatus();
        return status.equals("ProductOwner");
    }
    
    public void setIsAllowedToCompletedTasks(boolean value)
    {
        //nothing to do here? :-(
    }
    public boolean getIsNotAllowedToScrumReview()
    {
        String status = lu.getUser().getStatus();
        return status.equals("ProductOwner");
    }
    
    public void setIsAllowedToScrumReview(boolean value)
    {
        //nothing to do here? :-(
    }
}
